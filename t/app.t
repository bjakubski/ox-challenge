#!/usr/bin/env perl
use strict;
use warnings;
use autodie qw/:all/;
use FindBin qw/$Bin/;
use lib "$Bin/../lib";

use IPC::Cmd qw/can_run/; 
use File::Temp qw/tempdir/;
use Test::Mojo;
use Test::More;
use Test::Deep;



my $tmp_dir = tempdir( CLEANUP => 1);
my $db_file = "$tmp_dir/db.sqlite";

# TODO most primitive way of setting db up (also requires sqlite program
# installed
if (not can_run("sqlite3")) {
    die "Tests require 'sqlite3' binary somewhere in PATH, usually it is in 'sqlite' package";
}
system("sqlite3 $db_file <$Bin/../tables.sql");

use DNSApp;
my $app = DNSApp->new(db_filepath => $db_file);
my $t = Test::Mojo->new($app);
sub dump_res {#{{{
        diag $t->tx->res->to_string;
}#}}}

subtest "Some checks for non-existing endpoints", sub {
    $t->get_ok('/')->status_is(404);
    $t->get_ok('/some-string')->status_is(404);
    $t->get_ok('/dns/nonexistent')->status_is(404);
};

my $test_a_record = {
    type   => 'a',
    domain => 'example.com',
    target => '1.2.3.4',
};
my $test_a_record_2 = {
    type   => 'a',
    domain => 'example.org',
    target => '10.2.3.4',
};
subtest "Basic A record tests", sub {#{{{
    $t->get_ok('/dns')
        ->status_is(200)
        ->json_is('', [], "no records so far, thus empty response")
    ;
    $t->get_ok('/dns/a')
        ->status_is(200)
        ->json_is('', [], "no A records so far, thus empty response")
    ;
    $t->get_ok('/dns/cname')
        ->status_is(200)
        ->json_is('', [], "no CNAME records so far, thus empty response")
    ;

    note "POST in first A record";
    $t->post_ok('/dns/a/'.$test_a_record->{domain},
        form => {
            target => $test_a_record->{target},
        }
    )   
        ->status_is(200, "POSTing A type record seems fine")
        ->json_has('/status', 'OK')
    ;

    $t->get_ok('/dns')
        ->status_is(200)
        ->json_is('', [$test_a_record], "GET /dns returns our new A record!")
    ;
    $t->get_ok('/dns/a')
        ->status_is(200)
        ->json_is('', [$test_a_record], "GET /dns/a returns our new A record!")
    ;
    $t->get_ok('/dns/cname')
        ->status_is(200)
        ->json_is('', [], "GET /dns/cname still does not see anything")
    ;
    
    note "POST in second A record";
    $t->post_ok('/dns/a/'.$test_a_record_2->{domain},
        form => {
            target => $test_a_record_2->{target},
        }
    )->json_has('/status', 'OK');
    $t->get_ok('/dns')
        ->status_is(200)
    ;
    # When Mojo::Test json support is not enough we still can use normal tools
    cmp_deeply(
        $t->tx->res->json,
        bag(
            $test_a_record,
            $test_a_record_2,
        ),
        "Both records are returned after GET /dns",
    );
    $t->get_ok('/dns/a')
        ->status_is(200)
    ;
    cmp_deeply(
        $t->tx->res->json,
        bag(
            $test_a_record,
            $test_a_record_2,
        ),
        "Both records are returned after GET /dns/a",
    );

    note "Check if we can update existing record";
    $test_a_record->{target} = "192.2.3.4";

    $t->put_ok('/dns/a/' . $test_a_record->{domain},
        form => {
            target => $test_a_record->{target},
        }
    )->status_is(200);
    
    $t->get_ok('/dns/a')
        ->status_is(200)
    ;
    cmp_deeply(
        $t->tx->res->json,
        bag(
            $test_a_record,
            $test_a_record_2,
        ),
        "After PUT update updated record looks... well, updated",
    );

    # TODO add test that checks if record type change works (it does)
    #
    $t->delete_ok('/dns/cname/'.$test_a_record_2->{domain})
        ->status_is(404, "DELETE on wrong record type fails")
        ->json_has('/status', 'ERROR'),
    ;

    $t->delete_ok('/dns/a/'.$test_a_record_2->{domain})
        ->status_is(200, "DELETE succeeded")
        ->json_has('/status', 'OK'),
    ;
    $t->get_ok('/dns/a')
        ->status_is(200)
        ->json_is('', [$test_a_record], "GET /dns/a returns our remaining A record")
    ;


    $t->post_ok('/dns/a/somedomain.x',
        form => {
        }
    )   
        ->status_is(403, "Attempting to store record without data fails")
        ->json_has('/status', 'ERROR')
    ;



};#}}}

subtest "Check CNAME functionality", sub {#{{{

    my $cname_record = {
        type => 'cname',
        domain => 'myalias.net',
        target => $test_a_record->{domain},
    };
    $t->post_ok('/dns/cname/myalias.net',
        form => {
            target => 'nonexistent.com'
        },
    )
        ->status_is(400, "Can't add CNAME to domain we do not know")
        ->json_is('/status', 'ERROR')
        ->json_is('/message', "target domain does not exist")
    ;

    $t->post_ok('/dns/cname/' . $cname_record->{domain},
        form => {
            target => $test_a_record->{domain},
        },
    )->status_is(200, "Created our first CNAME");

    $t->get_ok('/dns')
        ->status_is(200)
    ;
    cmp_deeply(
        $t->tx->res->json,
        bag(
            $test_a_record,
            $cname_record,
        ),
        "/dns return our both CNAME and A records",
    );
    $t->get_ok('/dns/cname')
        ->status_is(200)
        ->json_is(
            '',
            [ $cname_record ],
            "GET /dns/cname sees our CNAME record!",
        )
    ;


    $t->delete_ok('/dns/a/'.$test_a_record->{domain})
        ->status_is(403)
        ->json_is('', { 
                status => 'ERROR',
                message => "Can't delete record, as there is at least one CNAME record pointing to it",
                cnames => [$cname_record->{domain}],
        })

    ;
    # TODO Test CNAME pointing to CNAME in similar way
};#}}}


done_testing;
