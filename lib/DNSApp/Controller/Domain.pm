package DNSApp::Controller::Domain;

use Mojo::Base 'Mojolicious::Controller';

use Method::Signatures::Simple;

my %json_ok = (json => {status => 'OK'});
my %status_ok = (status => 200, %json_ok);

method get_domains {
    my $type = $self->param("type");

    # Our model happens to return exact data structure we need!
    return $self->render(
        json => [
            $self->model->get_domains(
                defined $type ? (type => $type) : ()
            )
        ]
    );
}

method set_domain {
    my $domain = $self->param("domain");
    my $type   = $self->param("type");
    my $target = $self->param("target");

    if (not defined $target or length($target) == 0) {
        return $self->render(
            status => 403,
            json => {status => 'ERROR', message => '"target" parameter must be present and non-empty'}
        );
    }

    my ($success, $err) = $self->model->set_domain($domain, $type, $target);
    if (not $success) {
        my $msg = $err->{message};
        # Something we know? Convert code to message
        if ($msg eq 'TARGET_DOES_NOT_EXIST') {
            $msg = 'target domain does not exist';
        }
        return $self->render(
            json    => {status => 'ERROR', message => $msg},
            status  => 400,
        );
    }
    return $self->render( %status_ok );
}

method delete_domain {
    my $domain = $self->param("domain");
    my $type = $self->param("type");

    my $existing = $self->model->get_domain($domain, $type);
    if (not $existing) {
        return $self->render(status => 404, json => {status => 'ERROR'});
    }
    my ($success, $err) = $self->model->delete_domain( $domain );
    if (not $success) {
        my $message = $err->{message};
        my %additional = ();
        # Something we know? Convert code to message
        if ($message eq 'USED_AS_TARGET') {
            $additional{cnames} = [ map { $_->{domain} } @{ $err->{cnames} } ];
            $message = "Can't delete record, as there is at least one CNAME record pointing to it";
        }
        return $self->render(
            json    => {status => 'ERROR', message => $message, %additional},
            status  => 403,
        );
    }

    return $self->render( %status_ok );

}


1;
