package DNSApp::Model::SQLite;
use Moo;
use DBI;
use SQL::Abstract;
use Method::Signatures::Simple;
# NOTE Seems that Mojo::SQLite could be useful here (instead of bare DBI calls,
# for some convenience, less code and some features like "migrations", which
# would allow us to keep initial DB structure statements here

has filepath => (
    is => 'ro',
);

has _dbh => (
    is  => 'ro',
    lazy => 1,
    default => sub {
        # TODO RaiseError etc.
        DBI->connect("dbi:SQLite:dbname=".$_[0]->filepath, q{},q{});
    }
);

has _sqla => ( is => 'ro', lazy => 1, default => sub { SQL::Abstract->new } );


method get_domain($domain, $type) {#{{{
    return $self->get_domains( type => $type, domain => $domain );
}#}}}

method get_domains( %params ) {#{{{
    my ($sql, @bind) = $self->_sqla->select(
        'domains', '*', \%params,
    );
    return @{ 
        $self->_dbh->selectall_arrayref(
            $sql,
            { Slice => {} },
            @bind,
        ) // [] }
}#}}}

method set_domain($domain, $type, $target) {#{{{
    # In case of CNAME make sure target domain exists
    if ($type eq 'cname') {
        if (not  $self->_get_domain($target)) {
            return (undef, {message => 'TARGET_DOES_NOT_EXIST'});
        }
    }

    my $current = $self->_get_domain($domain);
    # TODO is there UPSERT in SQLite?
    if (not $current) {
        $self->_dbh->do(
            'INSERT INTO domains(domain, type, target) VALUES(?,?,?)',
            {},
            $domain, $type, $target
        );
    } else {
        $self->_dbh->do(
            'UPDATE domains SET type = ?, target = ? WHERE domain = ?',
            {},
            $type, $target, $domain
        );
    }

    return(1, undef);
}#}}}

method delete_domain($domain, $type) {#{{{
    # Check if domain being deleted is a target for some CNAME
    if ( my @cnames = $self->get_domains_that_target( $domain ) ) {
        return( 0, { message => 'USED_AS_TARGET', cnames => \@cnames});
    }
    $self->_dbh->do(
        'DELETE FROM domains WHERE type = ? AND domain = ?',
        {},
        $type, $domain
    );
    return (1, undef);
}#}}}

method get_domains_that_target($target) {#{{{
    # Only CNAME can point to some other domain
    return $self->get_domains( target => $target, type => 'cname');
}#}}}

method _get_domain($domain) {#{{{
    return $self->get_domains( domain => $domain );
}#}}}

1;
