package DNSApp;

# enables use strict/warnings
use Mojo::Base 'Mojolicious';

# This allows us to easily override that value in test with temporary file
has 'db_filepath' => 'db.sqlite';

use Method::Signatures::Simple;

use DNSApp::Model::SQLite;

method startup {
    # Not really important, as we do not use signed cookies
    $self->secrets(['vbbfbusdfnvbjkfbn dflbdbd']);


    # NOTE If needed we can make model class configurable via config file (built in
    # in Mojolicious) and have for example simple "mock" model using in-memory
    # data structure which can be used in tests without needing any sqlite file
    $self->helper(
        model => sub {
            state $m = DNSApp::Model::SQLite->new(filepath => $self->db_filepath);
        }
    );

    my $r = $self->routes;
    $r->namespaces(['DNSApp::Controller']);

    # Used to validate allowed types
    my %types = (type => ['a', 'cname']);

    $r->get(   '/dns')->to('Domain#get_domains');
    $r->get(   '/dns/:type'         => [%types])->to('Domain#get_domains');
    $r->post(  '/dns/:type/#domain' => [%types])->to('Domain#set_domain');
    $r->put(   '/dns/:type/#domain' => [%types])->to('Domain#set_domain');
    $r->delete('/dns/:type/#domain' => [%types])->to('Domain#delete_domain');
}

1;
