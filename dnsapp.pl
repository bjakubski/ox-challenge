#!/usr/bin/env perl
use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/lib";
use lib "$Bin/perl5/lib/perl5";

require Mojolicious::Commands;
Mojolicious::Commands->start_app('DNSApp');
