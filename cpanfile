requires 'IPC::System::Simple'; # for autodie :all
requires 'DBD::SQLite';
requires 'Method::Signatures::Simple';
requires 'Mojolicious';
requires 'Moo';
requires 'SQL::Abstract';
requires 'Test::Deep';
