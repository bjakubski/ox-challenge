﻿# DNSApp

This webservice allows you to store, retrieve and delete A and CNAME DNS records.

### General response information
Calls that modify data (POST/PUT/DELETE) will return JSON object as a response body. This object will always contain `status` property which can be of value either `OK` or `ERROR`. This should be used to detect success or failure of operation. Appropriate HTTP status codes should also be returned.
In case of `status` being `ERROR` there *may* be an `message` property with detailed description of encountered error. Specific calls may put other properties in this JSON object with additional data about problem. Please refer to specific calls documentation for details.


## Retrieving records

You can retrieve records by issuing GET HTTP requests to one of following endpoints:

- `/dns` - retrieves all stored DNS records
- `/dns/a` - retrieves all A DNS records
- `/dns/cname` - retrieves all CNAME DNS records

Returned data is a JSON array, where every element is an object with three properties:
	- `domain` is the name of the record (domain)
	- `type` is type of the record. It can be one of `cname` or `a`
	- `target` contains record data

Example request:
```
$ curl $URL/dns
[{"domain":"example.com","target":"192.2.3.4","type":"a"},{"domain":"example.org","target":"10.2.3.4","type":"a"},{"domain":"myalias.net","target":"example.com","type":"cname"}]
$ curl $URL/dns/cname
[{"domain":"myalias.net","target":"example.com","type":"cname"}]
```

## Storing and updating records
To store or update existing record you have to issue a POST or PUT HTTP method request.
To store an A record for domain `example.com` you should perform request to `/dns/a/example.com` URL and similarly for CNAME records you should use `/dns/cname/example.com`.
To specify record data you must pass `target` parameter in request body. This parameter is mandatory and it cannot be empty.

For example to store 2.2.2.2 as a record data for A record for domain abc.ie:
```
$ curl -X POST $URL/dns/a/abc.ie --data target=2.2.2.2
```
Note that there can be only one record stored for any domain (i.e. it is not possible to have multiple A records for single domain not you cannot have CNAME and A record at the same time.

If record being is a `cname` then record specified as a target **must** already exist in webservice. Otherwise 

If you want to update existing record then just perform same request as when creating new record. If there is any kind of record for specified domain it will be updated. It is also possible to  change report type that way, so following sequence
```
$ curl -X POST $URL/dns/a/foobar.pl --data target=1.1.1.1
$ curl -X POST $URL/dns/cname/foobar.pl --data target=1.1.1.1
```
will result in following record stored for `foobar.pl`:
```
    {
        "domain": "foobar.pl",
        "target": "1.1.1.1",
        "type": "cname"
    }
```

### Notes
* PUT and POST requests are considered equivalent when storing/updating data

## Deleting data
To delete record issue a HTTP DELETE method request to URI specifying seelcted record type and domain, for example:
```
$ curl -X DELETE $URL/dns/cname/foobar.pl
```
In order for delete to succeed record for specified domain must exist and must be of the same type as one specified in request.
It is not possible to remove any record for which there exists CNAME type record pointing to!
For example:
```
$ curl -X POST $URL/dns/a/a.pl --data target=5.7
$ curl -X POST $URL/dns/cname/b.pl --data a.pl
# b.pl is now a CNAME to a.pl
$ $ curl -X DELETE $URL/dns/a/a.pl
{"cnames":["b.pl"],"message":"Can't delete record, as there is at least one CNAME record pointing to it","status":"ERROR"}%
```
In such case appropriate error message is provided in `message` response JSON and `cname` property is an array of CNAME records that prevented DELETE operation from succeeding.



